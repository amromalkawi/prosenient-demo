package com.example.prosentient.demo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bibliographic {
private String copyrightdate;
    private String isbn;
    private String author;
    @JsonProperty("Subjects")
    private String subjects;
    private String title;
    private String biblionumber;
    private String type;
}
