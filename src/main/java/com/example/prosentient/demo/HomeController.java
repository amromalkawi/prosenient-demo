package com.example.prosentient.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class HomeController {

    final String uri = "https://prosentient.intersearch.com.au/cgi-bin/koha/svc/report?id=2&annotated=1";

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/")
    public String home(Model model) throws JsonProcessingException {
       ResponseEntity<List<Bibliographic>> rateResponse =
                restTemplate.exchange(uri,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Bibliographic>>() {
                        });
        List<Bibliographic> Bibliographics = rateResponse.getBody();
        model.addAttribute("list", Bibliographics);
        System.out.println(Bibliographics);
        return "home";
    }
}









































//didn't you face any cross domain issues? I am calling an api hosted somewhere else from localhost and it is giving cross domain issues. – Harit Vishwakarma May 30 '18 at 11:13
//        I am also face same cors issue..plz help – Nitin Wahale Aug 13 '18 at 10:45
//@HaritVishwakarma - it will if the api you are calling doesn't have Access-Control-Allow-Origin for your domain(localhost). Try creating your own proxy, send req to the proxy and forward the request to your destination. Since this will be a server to server communication, the request wont be blocked(CORS is blocked by browsers). Send back this response with the allow-origin header set to all – sss999 Mar 11 at 20:04
//@HaritVishwakarma and NitinWahale and future devs you can disable web security on your local browser for testing purposes only though - this won't work as a production solution. Ref here: stackoverflow.com/questions/3102819/… – KDT Apr 29 at 14:56
